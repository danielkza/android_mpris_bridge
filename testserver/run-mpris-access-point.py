import dbus
import dbus.service
from dbus.mainloop.glib import DBusGMainLoop
import glib
import gobject
from tcpservergtk import GlibJSONRPCServer
import jsonrpclib
import ConfigParser
from gtk import icon_theme_get_default, ICON_LOOKUP_USE_BUILTIN
import base64
import cStringIO as StringIO
import avahi
import urllib2
from encodings.idna import ToASCII
import traceback

class DBUSHelper:

	def __init__(self,target_bus):
	
		self.bus = target_bus

	def get_properties( self, server, dbus_obj_path ):

		dbus_object = self.bus.get_object(server,dbus_obj_path)
		properties_manager = dbus.Interface(dbus_object, 'org.freedesktop.DBus.Properties')   
	 
		return properties_manager

class MprisAccessPointAvahiHelper:

	def __init__(self,system_bus):

		self.bus = system_bus
		self.serviceType = "_mprisap._tcp"
		self.servicePort = 3338
		self.serviceTXT = "mprisap"
		self.domain = "local"
		self.host = ""
		self.group = None
		self.rename_count = 12
	
		self.groups = {}

		self.busHelper = DBUSHelper(system_bus)

		self.avahiSrv = dbus.Interface(self.bus.get_object( avahi.DBUS_NAME, avahi.DBUS_PATH_SERVER ), avahi.DBUS_INTERFACE_SERVER )
		self.__server_state_changed( self.avahiSrv.GetState() )
		self.avahiSrv.connect_to_signal( "StateChanged", self.__server_state_changed )

	def shutdown(self):

		self.__remove_service()

	def __exit__(self, type, value, traceback):

		self.__remove_service()

	def __get_host_identity(self):

		props = self.busHelper.get_properties('org.freedesktop.hostname1', '/org/freedesktop/hostname1')
		return str(props.Get('org.freedesktop.hostname1','Hostname'))

	def encode_dns(self, name):
		out = []
		for part in name.split('.'):
			if len(part) == 0: continue
			out.append(ToASCII(part))
		return '.'.join(out)

	def createRR(self, name):
		out = []
		for part in name.split('.'):
			if len(part) == 0: continue
			out.append(chr(len(part)))
			out.append(ToASCII(part))
		out.append('\0')
		return ''.join(out)	

	def add_service(self):

		self.avahi_group = self.avahiSrv.EntryGroupNew()

		self.group = dbus.Interface(
				self.bus.get_object( avahi.DBUS_NAME, self.avahi_group),
				avahi.DBUS_INTERFACE_ENTRY_GROUP)
		self.group.connect_to_signal('StateChanged', self.__entry_group_state_changed)


		self.serviceName = "MPRIS AP "+self.__get_host_identity()
		print "Adding service '%s' of type '%s' ..." % (self.serviceName, self.serviceType)

		self.group.AddService(
				avahi.IF_UNSPEC,    #interface
				avahi.PROTO_UNSPEC, #protocol
				avahi.PUBLISH_UPDATE|avahi.PUBLISH_USE_MULTICAST, #flags
				self.serviceName, self.serviceType,
				self.domain, self.host,
				dbus.UInt16(self.servicePort),
				avahi.string_array_to_txt_array(self.serviceTXT))

		self.group.Commit()

	def __remove_service(self):

		if not self.group is None:
			self.group.Reset()
			self.group = None

	def __server_state_changed(self,state):

		if state == avahi.SERVER_COLLISION:
			print "WARNING: Server name collision"
			self.__remove_service()
		elif state == avahi.SERVER_RUNNING:
			self.add_service()

	def __entry_group_state_changed(self,state, error):

		print "state change: %i" % state

		if state == avahi.ENTRY_GROUP_ESTABLISHED:
			print "Service established."
		elif state == avahi.ENTRY_GROUP_COLLISION:

			self.rename_count = self.rename_count - 1
			if self.rename_count > 0:
				self.serviceName = server.GetAlternativeServiceName(name)
				print "WARNING: Service name collision, changing name to '%s' ..." % name
				self.__remove_service()
				self.add_service()

			else:
				print "ERROR: No suitable service name found after %i retries, exiting." % n_rename
				main_loop.quit()

		elif state == avahi.ENTRY_GROUP_FAILURE:
			print "Error in group state changed", error
			main_loop.quit()
			return

class MprisBaseInterfaceProxy:

	def __init__(self, server, target_bus, mpris_iface):

		self.server = server
		self.bus = target_bus
		self.dbus_helper = DBUSHelper(self.bus)

		if mpris_iface is not None:
			self.iface = "org.mpris.MediaPlayer2."+mpris_iface
		else:
			self.iface = "org.mpris.MediaPlayer2";

	@property
	def mpris_iface( self ):

		dbus_object = self.bus.get_object(self.server,'/org/mpris/MediaPlayer2')
		iface = dbus.Interface(dbus_object, self.iface)

		return iface

	def setprop( self, prop, value ):

		props = self.dbus_helper.get_properties(self.server, '/org/mpris/MediaPlayer2')
		props.Set(self.iface, prop, value) 

	def getprop( self, prop ):

		props = self.dbus_helper.get_properties(self.server, '/org/mpris/MediaPlayer2')
		return props.Get(self.iface, prop) 

class MprisMprisProxy(MprisBaseInterfaceProxy):

	def __init__(self, server, target_bus):
		MprisBaseInterfaceProxy.__init__(self, server, target_bus, None)

	@property
	def identity(self):
		return str(self.getprop("Identity"))

	@property
	def icon(self):
		desktopFile = str(self.getprop("DesktopEntry"))
		fullPath = "/usr/share/applications/"+desktopFile+".desktop"
		config = ConfigParser.ConfigParser()
		config.read(fullPath)
		icon = config.get("Desktop Entry","Icon")
		icon_file = self.__load_icon(128, icon)
		io = StringIO.StringIO()
		icon_file.save_to_callback(io.write, 'png')
		buffer = io.getvalue()
		return base64.b64encode(buffer) 

	def __load_icon(self, size, *alternatives):

		    it = icon_theme_get_default()
		    alternatives = list(alternatives)
		    name = alternatives.pop(0)

		    try:
			return it.load_icon(name, size, ICON_LOOKUP_USE_BUILTIN)
		    except:
			if alternatives:
			    return load_icon(size, *alternatives)



class MprisPlayerProxy(MprisBaseInterfaceProxy):

	def __init__(self, server, target_bus):
		MprisBaseInterfaceProxy.__init__(self, server, target_bus, "Player")
		self.method_mapping = { 'playPause':self.play_pause,'next':self.go_next,'previous':self.go_previous,'volume':self.volume,'metadata':self.metadata, 'playbackstatus':self.playbackstatus }
	
	def __getattr__(self,name):
		if name in self.method_mapping:
			return self.method_mapping[name]	
		else:
			return AttributeError 

	def volume(self, d):
		self.setprop("Volume",d)

	@property
	def metadata(self):
		return self.getprop("Metadata")	

	@property
	def playbackstatus(self):
		return self.getprop("PlaybackStatus")

	def play_pause(self):
		self.mpris_iface.PlayPause()

	def go_next(self):
		self.mpris_iface.Next()
		
	def go_previous(self):
		self.mpris_iface.Previous()

class MprisRpcDispatcher():

	def __init__(self,ap):

		self.ap = ap
 
	def _dispatch(self, method, args):

		print "Dispatching " + method

		splitted = method.split(".")

		if splitted[0] == "mprisap":
			func = getattr(self.ap, splitted[1])
			return func(*args)	
		elif splitted[0] == "server":
			print "server"
			for server in self.ap.getMonitors():
				print "server: '"+server+"' : '"+splitted[1]+"'"
				if splitted[1] == server:
					for monitor in self.ap.getMonitors()[server]:
						print "Monitor to: " + monitor.getOrigin()
						if splitted[2] == "mpris":
							obj = MprisMprisProxy("org.mpris.MediaPlayer2."+server, self.ap.getBus())
							func = getattr(obj, splitted[3])
							return func(*args)
						elif splitted[2] == "player":
							obj = MprisPlayerProxy("org.mpris.MediaPlayer2."+server, self.ap.getBus())
							func = getattr(obj, splitted[3])
							return func(*args)

class MprisServerMonitor:

    def __init__(self,server,origin,ap):

		print "MprisServerMonitor of " + server + " for " + origin

		self.ap = ap
		self.bus = dbus.SessionBus()
		self.server = str(server)
		self.origin = origin

		mpris_proxy = MprisMprisProxy(server, self.bus)
		player_proxy = MprisPlayerProxy(server, self.bus) 

		playbackstatus = player_proxy.playbackstatus

		if playbackstatus is not None:
			self.__handle_playbackstatus(playbackstatus)	

		props = DBUSHelper(dbus.SystemBus()).get_properties('org.freedesktop.hostname1', '/org/freedesktop/hostname1')
		hostname = str(props.Get('org.freedesktop.hostname1','Hostname'))

		identity_with_hostname = mpris_proxy.identity + " (on " + hostname + ")"
		jsonServer = jsonrpclib.Server("http://"+self.origin+":3339")
		jsonServer.mpris.onIdentity(self.server, identity_with_hostname, mpris_proxy.icon)

		metadata = player_proxy.metadata

		if metadata is not None:
			self.__handle_metadata(metadata)
	
		dbus_object = self.bus.get_object(server, '/org/mpris/MediaPlayer2')
		properties_manager = dbus.Interface(dbus_object, 'org.freedesktop.DBus.Properties')   
		self.prop_match = properties_manager.connect_to_signal("PropertiesChanged", self.__handle_properties_changed)

    def shutdown(self):
		self.prop_match.remove()
		self.origin = None

    def getOrigin(self):
		return self.origin

    def getServer(self):
		return self.server

    def __handle_properties_changed(self, interface, changed, invalid):

		if self.origin is None:
			return

		if interface == "org.mpris.MediaPlayer2.Player":
			if "Metadata" in changed:
				metadata = changed.get("Metadata", None)
				if metadata is not None:
					self.__handle_metadata(metadata)
			
			if "PlaybackStatus" in changed:
				playbackstatus = changed.get("PlaybackStatus", None)
				if playbackstatus is not None:
					self.__handle_playbackstatus(playbackstatus)

    def __handle_playbackstatus(self, playbackstatus):

		jsonSrv = jsonrpclib.Server("http://"+self.origin+":3339")
		jsonSrv.mpris.onPlaybackStatus(self.server, str(playbackstatus))
	
    def __handle_metadata(self, metadata):

		print metadata

		self.old_metadata = metadata

		title = metadata.get("xesam:title")
		album = metadata.get("xesam:album")
		artist = metadata.get("xesam:artist")
		artUrl = metadata.get("mpris:artUrl")

		jsonSrv = jsonrpclib.Server("http://"+self.origin+":3339")
		
		if title is not None and artist is not None:

			try:
				jsonSrv.mpris.onMetadata(self.server, str(title),str(artist[0]))
			except:
				pass

		if artUrl is not None:

			try:
				if artUrl[:4]=="http":
					print "Artwork http: "+artUrl
					img = urllib2.urlopen(artUrl).read()
					jsonSrv.mpris.onCoverArt(self.server, base64.b64encode(img))
				elif artUrl[:4]=="file":
					print "Artwork file: "+urllib2.unquote(artUrl[7:])
					with open(urllib2.unquote(artUrl[7:]), mode='rb') as file:
						img = file.read()
						jsonSrv.mpris.onCoverArt(self.server, base64.b64encode(img))
			except:
				pass	



class MprisAccessPoint:

	def __init__(self):

	    self.monitors = {}
	    self.origin = None

	    self.bus = dbus.SessionBus()
	    self.ah = MprisAccessPointAvahiHelper(dbus.SystemBus())
	    self.b = self.bus.get_object('org.freedesktop.DBus','/org/freedesktop/DBus')
	    self.i = dbus.Interface(self.b, 'org.freedesktop.DBus')

	    self.i.connect_to_signal( "NameOwnerChanged", self.__bus_name_owner_changed )

	    self.mpris_rpc_dispatcher = MprisRpcDispatcher(self)

	    self.__init_rpc()

	def getMonitors(self):
	    return self.monitors

	def getBus(self):
	    return self.bus

	def getRpc(self):
            return self.rpc

	def shutdown(self):
	    self.ah.shutdown()

	def __bus_name_owner_changed(self,name,old_owner,new_owner):

	    if self.origin is not None and name[:22]=="org.mpris.MediaPlayer2":

		    jsonServer = jsonrpclib.Server("http://"+self.origin+":3339")

		    server = str(name)
		    real = server[23:]

		    if new_owner == '':
	

			    #TODO: Signal this to the origin; which also should be fault-tolerant regarding this and similar events
			    for monitor in self.ap.getMonitors()[real]:
				if monitor.getOrigin().equals(self.origin):
					monitor.shutdown()
					self.ap.getMonitors()[real].remove(monitor)				

			    jsonServer.mprisap.onServerRemoved(server)

		    elif old_owner == '':
			    self.monitors[real] = []
			    mpris_proxy = MprisMprisProxy(server, self.bus)
			    jsonServer.mprisap.onServerAdded(server, mpris_proxy.identity, mpris_proxy.icon)
			    print "Server " + mpris_proxy.identity + " added"

	def discover(self,origin):

	    services = self.i.ListNames()
 	    jsonServer = jsonrpclib.Server("http://"+origin+":3339")

	    for s in services:
		if str(s).startswith('org.mpris.MediaPlayer2'):
			try:
				server = str(s)
				self.monitors[server[23:]] = []
				mpris_proxy = MprisMprisProxy(server, self.bus)
				jsonServer.mprisap.onServerAdded(server, mpris_proxy.identity, mpris_proxy.icon)
			except:
				pass

	def removeMonitor(self,server,origin):

	    real = str(server[23:]) 
	    print "Removing monitors for "+real+" with origin "+origin
	
            for monitor in self.monitors[real]:
		monitor.shutdown()

	    l = self.monitors[real]
	    del l
            self.monitors[real] = []

	    del self.monitors[real]

	def addMonitor(self,server,origin):

	    real = str(server[23:]) 
	    print "Adding monitor for "+real+" with origin "+origin

	    try:
		    monitor = MprisServerMonitor(server,origin,self)

		    if real not in self.monitors:
			self.monitors[real] = []

		    self.monitors[real].append(monitor)

		    print "...monitor added" 
	    except TypeError, e:
		    traceback.print_exc()

	def watch(self, origin):

	    self.origin = origin

	def ignore(self, origin):

	    if(self.origin == origin):
		self.origin = None

	def __init_rpc(self):

	    self.rpc = GlibJSONRPCServer(('0.0.0.0', 3338))
	    self.rpc.add_allowed_ip('255.255.255.255')

	    self.rpc.register_instance(self.mpris_rpc_dispatcher)

def main():

	DBusGMainLoop(set_as_default=True)

	ap = MprisAccessPoint()

	try:
		gobject.MainLoop().run()
	except KeyboardInterrupt:
		pass

	ap.shutdown()

if __name__ == "__main__":

	main()
