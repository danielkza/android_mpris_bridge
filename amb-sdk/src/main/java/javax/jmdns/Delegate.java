package javax.jmdns;

import java.util.Collection;

/**
 *
 */
public interface Delegate {

    /**
     * This method is called if JmDNS cannot recover from an I/O error.
     *
     * @param dns
     *            target DNS
     * @param infos
     *            service info registered with the DNS
     */
    public void cannotRecoverFromIOError(JmDNS dns, Collection<ServiceInfo> infos);

}
