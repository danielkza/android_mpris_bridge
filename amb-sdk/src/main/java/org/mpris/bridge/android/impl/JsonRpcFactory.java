package org.mpris.bridge.android.impl;

import org.json.rpc.client.HttpJsonRpcClientTransport;
import org.json.rpc.client.JsonRpcInvoker;
import org.mpris.bridge.android.interfaces.MprisAccessPointInterface;
import org.mpris.bridge.android.interfaces.MprisServerInterface;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by milosz on 02.10.14.
 */
public class JsonRpcFactory {

    private final static String REMOTE_IFACE_MPRIS_AP = "mprisap";
    private final static String REMOTE_IFACE_MPRIS_SERVICE = "mpris";

    public static MprisAccessPointInterface newAccessPointProxy(final String uri) throws MalformedURLException {

        HttpJsonRpcClientTransport transport;
        transport = new HttpJsonRpcClientTransport(new URL(uri));
        JsonRpcInvoker invoker = new JsonRpcInvoker();
        return invoker.get(transport, REMOTE_IFACE_MPRIS_AP, MprisAccessPointInterface.class);

    }

    public static MprisServerInterface newServiceProxy(final String uri, final String iface) throws MalformedURLException {

        HttpJsonRpcClientTransport transport;
        transport = new HttpJsonRpcClientTransport(new URL(uri));
        JsonRpcInvoker invoker = new JsonRpcInvoker();
        return invoker.get(transport, iface, MprisServerInterface.class);

    }

}
