package org.mpris.bridge.android.interfaces;

/**
* Created by milosz on 01.10.14.
*/
public interface MprisAccessPointBackchannelListener {

    public void onServerRemoved(String server);
    public void onServerAdded(String server, String identity, String iconBase64);

}
