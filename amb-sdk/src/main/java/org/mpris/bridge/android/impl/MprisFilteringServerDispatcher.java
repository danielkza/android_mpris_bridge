package org.mpris.bridge.android.impl;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Base64InputStream;

import org.mpris.bridge.android.interfaces.MprisServerBackchannelMarshaller;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

/**
 * Created by milosz on 02.10.14.
 */
public abstract class MprisFilteringServerDispatcher implements MprisServerBackchannelMarshaller {

    private final String server;

    public MprisFilteringServerDispatcher(final String server) {
        this.server = server;
    }

    @Override
    public void onCoverArt(String server, String coverArtBase64) {
        if(server.equals(this.server)) {

            InputStream stream = null;
            try {
                stream = new ByteArrayInputStream(coverArtBase64.getBytes("utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            Base64InputStream is = new Base64InputStream(stream, Base64.DEFAULT);

            final Bitmap coverArt = BitmapFactory.decodeStream(is);

            gotCoverArt(coverArt);
        }
    }

    @Override
    public final void onMetadata(String server, String title, String artist) {
        if(server.equals(this.server)) {
            gotMetadata(title, artist);
        }
    }

    @Override
    public final void onPlaybackStatus(String server, String playbackStatus) {
        if(server.equals(this.server)) {
            gotPlaybackStatus(playbackStatus);
        }
    }

    @Override
    public final void onIdentity(String server, String identity, String iconBase64) {
        if(server.equals(this.server)) {

            InputStream stream = null;
            try {
                stream = new ByteArrayInputStream(iconBase64.getBytes("utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            Base64InputStream is = new Base64InputStream(stream, Base64.DEFAULT);

            final Bitmap icon = BitmapFactory.decodeStream(is);

            gotIdentity(identity, icon);
        }
    }
}
