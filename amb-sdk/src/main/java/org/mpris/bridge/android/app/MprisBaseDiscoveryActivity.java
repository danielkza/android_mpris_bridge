package org.mpris.bridge.android.app;

import android.app.Activity;
import android.content.res.Configuration;

import org.mpris.bridge.android.Constants;
import org.mpris.bridge.android.impl.RpcBackchannelInfo;
import org.mpris.bridge.android.proxy.MprisAccessPointProxy;
import org.mpris.bridge.android.interfaces.MprisAccessPointBackchannelListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by milosz on 03.10.14.
 */
public abstract class MprisBaseDiscoveryActivity extends Activity {

    private MprisAccessPointProxy proxy;
    private boolean resumed = false;

    protected abstract MprisAccessPointBackchannelListener getAccessPointListener();

    public MprisBaseDiscoveryActivity() {
    }

    public final MprisAccessPointProxy getProxy() {
        return proxy;
    }

    public final boolean switchProxy(final String apUrl) {

        if (proxy != null) {
            proxy.stop();
            proxy.shutdown();
        }

        final RpcBackchannelInfo info =
                new RpcBackchannelInfo(getAccessPointListener(),
                        MprisAccessPointBackchannelListener.class,
                        Constants.MPRIS_RPC_AP_INTERFACE);

        final List<RpcBackchannelInfo> infos =
                new ArrayList<RpcBackchannelInfo>();

        infos.add(info);

        try {
            proxy = new MprisAccessPointProxy(getApplicationContext(), infos, apUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (resumed)
            proxy.start();

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        resumed = true;

        if (proxy != null) {
            proxy.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        resumed = false;

        if (proxy != null) {
            proxy.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (proxy != null) {
            proxy.stop();
            proxy.shutdown();
        }
    }
}
