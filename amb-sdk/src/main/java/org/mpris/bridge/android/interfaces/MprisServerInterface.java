package org.mpris.bridge.android.interfaces;

/**
 * Created by milosz on 29.09.14.
 */
public interface MprisServerInterface {

    public void playPause();
    public void next();
    public void previous();
    public void volume(double volume);

}
