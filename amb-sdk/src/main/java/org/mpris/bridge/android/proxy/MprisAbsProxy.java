package org.mpris.bridge.android.proxy;

import android.content.Context;

import org.mpris.bridge.android.impl.MprisAccessPointBackchannel;
import org.mpris.bridge.android.impl.RpcBackchannelInfo;

import java.io.IOException;
import java.util.List;

/**
 * Created by milosz on 29.09.14.
 */
public class MprisAbsProxy {

    protected Context ctx;
    private MprisAccessPointBackchannel server;
    private String apUrl;

    public MprisAbsProxy(final Context ctx, List<RpcBackchannelInfo> infos,
                         final String apUrl) throws IOException {
        this.ctx = ctx;
        this.server = new MprisAccessPointBackchannel(infos);
        this.apUrl = apUrl;
    }

    public final Context getContext() {
        return ctx;
    }

    public final String getApUrl() {
        return apUrl;
    }

    public final void shutdown() {
        ctx = null;
    }

    public void start() {
        try {
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        if(server!=null) {
            server.stop();
        }

    }
}
