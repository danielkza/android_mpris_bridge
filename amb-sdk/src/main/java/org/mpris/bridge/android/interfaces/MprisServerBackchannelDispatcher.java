package org.mpris.bridge.android.interfaces;

import android.graphics.Bitmap;

/**
 * Created by milosz on 03.10.14.
 */
public interface MprisServerBackchannelDispatcher {

    public abstract void gotCoverArt(Bitmap coverArt);
    public abstract void gotMetadata(String title, String artist);
    public abstract void gotIdentity(String identity, Bitmap icon);
    public abstract void gotPlaybackStatus(String playbackStatus);

}
