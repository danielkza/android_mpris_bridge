package org.mpris.bridge.android.impl;

import android.accounts.NetworkErrorException;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v4.content.LocalBroadcastManager;
import android.text.format.Formatter;
import android.util.Log;

import org.json.rpc.commons.JsonRpcClientException;
import org.json.rpc.commons.JsonRpcRemoteException;
import org.mpris.bridge.android.Constants;
import org.mpris.bridge.android.exceptions.UnableToPerformDiscoveryException;
import org.mpris.bridge.android.interfaces.MprisAccessPointInterface;

import java.net.MalformedURLException;

/**
 * Created by milosz on 28.09.14.
 */
public class MprisAccessPointRpcService extends IntentService {

    private MprisAccessPointInterface proxy;

    public MprisAccessPointRpcService() {
        super("MprisAccessPointRpcService");
    }

    private String getOriginIp() throws NetworkErrorException {

        WifiManager wifiMgr = (WifiManager) getSystemService(WIFI_SERVICE);

        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

        if (wifiInfo != null) {
            return Formatter.formatIpAddress(wifiInfo.getIpAddress());
        }

        throw new NetworkErrorException();
    }

    public static void doAction(final Context ctx, final String action, final String apUrl) {

        Intent intent = new Intent(ctx, MprisAccessPointRpcService.class);
        intent.setAction(action);
        intent.putExtra(Constants.MPRIS_DATA_MPRISAP_URL, apUrl);
        ctx.startService(intent);

    }

    public static void doAction(final Context ctx, final String action, final String apUrl, final String server) {

        Intent intent = new Intent(ctx, MprisAccessPointRpcService.class);
        intent.setAction(action);
        intent.putExtra(Constants.MPRIS_DATA_SERVICE, server);
        intent.putExtra(Constants.MPRIS_DATA_MPRISAP_URL, apUrl);
        ctx.startService(intent);

    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if(intent==null||intent.getAction()==null)
            throw new IllegalArgumentException();

        try {
            final String uri = intent.getStringExtra(Constants.MPRIS_DATA_MPRISAP_URL);
            proxy = JsonRpcFactory.newAccessPointProxy(uri);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        final String action = intent.getAction();

        if(action.equals(Constants.MPRIS_AP_ACTION_DISCOVER)) {
            try {
                proxy.discover(getOriginIp());
            } catch( JsonRpcClientException e ) {
                e.printStackTrace();
            } catch( NetworkErrorException e ) {
                e.printStackTrace();
            } catch( JsonRpcRemoteException e ) {
                e.printStackTrace();
            }
        }
        else if(action.equals(Constants.MPRIS_AP_ACTION_MONITOR_SERVER)) {
            try {
                proxy.addMonitor(intent.getStringExtra(Constants.MPRIS_DATA_SERVICE), getOriginIp());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (NetworkErrorException e) {
                e.printStackTrace();
            } catch( JsonRpcRemoteException e ) {
                e.printStackTrace();
            }
        }
        else if(action.equals(Constants.MPRIS_AP_ACTION_REMOVE_MONITOR)) {
            try {
                proxy.removeMonitor(intent.getStringExtra(Constants.MPRIS_DATA_SERVICE), getOriginIp());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (NetworkErrorException e) {
                e.printStackTrace();
            } catch( JsonRpcRemoteException e ) {
                e.printStackTrace();
            }
        }
        else if(action.equals(Constants.MPRIS_AP_ACTION_WATCH)) {
            try {
                proxy.watch(getOriginIp());
            } catch( JsonRpcClientException e ) {
                e.printStackTrace();
            } catch (NetworkErrorException e) {
                e.printStackTrace();
            }
        }
        else if(action.equals(Constants.MPRIS_AP_ACTION_IGNORE)) {
            try {
                proxy.ignore(getOriginIp());
            } catch( JsonRpcClientException e ) {
                e.printStackTrace();
            } catch (NetworkErrorException e) {
                e.printStackTrace();
            }
        }

    }
}
