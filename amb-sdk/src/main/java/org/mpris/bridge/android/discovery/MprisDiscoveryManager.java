package org.mpris.bridge.android.discovery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;

import org.mpris.bridge.android.Constants;
import org.mpris.bridge.android.impl.MprisBonjourService;

/**
 * Created by milosz on 01.10.14.
 */
public class MprisDiscoveryManager {

    private Handler handler;

    private BroadcastReceiver receiver2 = new BroadcastReceiver() {

        @Override
        public void onReceive(
                Context context, Intent intent) {

            final MprisApHostInfo info = new MprisApHostInfo();

            info.address = intent.getStringExtra(Constants.MPRIS_DATA_ADDRESS);
            info.hostName = intent.getStringExtra(Constants.MPRIS_DATA_SERVICE);
            info.uri = Uri.parse(intent.getStringExtra(Constants.MPRIS_DATA_MPRISAP_URL));

            handler.post(new Runnable() {
                @Override
                public void run() {
                    discoveryListener.onAccessPointDiscovered(info);
                }
            });
        }
    };

    private BroadcastReceiver receiver3 = new BroadcastReceiver() {

        @Override
        public void onReceive(
                Context context, Intent intent) {

            final MprisApHostInfo info = new MprisApHostInfo();

            info.address = intent.getStringExtra(Constants.MPRIS_DATA_ADDRESS);
            info.hostName = intent.getStringExtra(Constants.MPRIS_DATA_SERVICE);

            handler.post(new Runnable() {
                @Override
                public void run() {
                    discoveryListener.onAccessPointDisappeared(info);
                }
            });
        }
    };


    public static class MprisApHostInfo {

        public String hostName;
        public String address;
        public Uri uri;

        @Override
        public boolean equals(final Object other) {
            if(!(other instanceof MprisApHostInfo))
                return false;

            MprisApHostInfo h2 = (MprisApHostInfo) other;

            if(address.equals(h2.address))
                return true;

            return false;
        }
    }

    public static interface DiscoveryListener {

        public void onDiscoveryStarted();
        public void onDiscoveryStopped();

        public void onAccessPointDiscovered(final MprisApHostInfo info);
        public void onAccessPointDisappeared(final MprisApHostInfo info);
    }

    private DiscoveryListener discoveryListener;

    public MprisDiscoveryManager(final Context ctx, final DiscoveryListener listener) {
        discoveryListener = listener;
        handler = new Handler(Looper.getMainLooper());
        MprisBonjourService.start(ctx);
    }

    public void shutdown(final Context ctx) {
        MprisBonjourService.stopDiscovery(ctx);
        MprisBonjourService.stop(ctx);
    }

    public void start(final Context ctx) {

        discoveryListener.onDiscoveryStarted();

        LocalBroadcastManager.getInstance(ctx).registerReceiver(receiver2,
                new IntentFilter(Constants.MPRIS_SERVICE_RESULT_AP_DISCOVERED));

        LocalBroadcastManager.getInstance(ctx).registerReceiver(receiver3,
                new IntentFilter(Constants.MPRIS_SERVICE_RESULT_AP_DISAPPEARED));

        MprisBonjourService.startDiscovery(ctx);


    }

    public void stop(final Context ctx) {

        LocalBroadcastManager.getInstance(ctx).unregisterReceiver(receiver2);
        LocalBroadcastManager.getInstance(ctx).unregisterReceiver(receiver3);

        discoveryListener.onDiscoveryStopped();
    }

}
