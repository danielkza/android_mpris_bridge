package org.mpris.bridge.android.interfaces;

/**
 * Created by milosz on 29.09.14.
 */
public interface MprisAccessPointInterface {

    public void discover(String origin);
    public void watch(String origin);
    public void ignore(String origin);
    public void addMonitor(String server, String origin);
    public void removeMonitor(String server, String origin);

}
