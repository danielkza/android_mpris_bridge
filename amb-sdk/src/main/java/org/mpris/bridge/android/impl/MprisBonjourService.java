package org.mpris.bridge.android.impl;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.*;
import android.os.Process;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.common.util.concurrent.AbstractExecutionThreadService;

import org.mpris.bridge.android.Constants;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceInfo;
import javax.jmdns.ServiceListener;
import javax.jmdns.ServiceTypeListener;

/**
 * Created by milosz on 03.10.14.
 */
public class MprisBonjourService extends Service {

    private final static String MPRIS_ACCESS_POINT_TYPE_NAME = "mprisap";

    private DiscoveryService srv;

    public static void start(final Context ctx) {

        Intent intent = new Intent(ctx, MprisBonjourService.class);
        ctx.startService(intent);

    }

    public static void stop(final Context ctx) {

        Intent intent = new Intent(ctx, MprisBonjourService.class);
        ctx.stopService(intent);

    }

    public static void startDiscovery(final Context ctx) {

        Intent intent = new Intent(ctx, MprisBonjourService.class);
        intent.setAction(Constants.MPRIS_DISCOVER_ACTION_START);
        ctx.startService(intent);

    }

    public static void stopDiscovery(final Context ctx) {

        Intent intent = new Intent(ctx, MprisBonjourService.class);
        intent.setAction(Constants.MPRIS_DISCOVER_ACTION_STOP);
        ctx.startService(intent);

    }


    private final Executor es = AsyncTask.THREAD_POOL_EXECUTOR;

    private ConnectionManager connectionMgr;

    private class DiscoveryService extends AbstractExecutionThreadService implements Handler.Callback {

        private final Object mutex = new Object();
        private volatile boolean running = false;
        private JmDNS mDNS;
        private HandlerThread handlerThread;
        private Looper looper;
        private Handler handler;

        @Override
        public boolean handleMessage(Message message) {

            stopDiscovery();
            initializeDiscovery();

            return true;
        }

        private class Listener2 implements ServiceListener {

            public void internalServiceAdded(final ServiceInfo info) {

                if(info.getApplication().equals(MPRIS_ACCESS_POINT_TYPE_NAME)) {

                    final String uri = info.getURLs()[0];

                    Log.d("MprisBonjour", "MPRIS Access Point: "+uri+" "+info.getName());

                    Intent intent = new Intent(Constants.MPRIS_SERVICE_RESULT_AP_DISCOVERED);
                    intent.putExtra(Constants.MPRIS_DATA_MPRISAP_URL, uri);
                    intent.putExtra(Constants.MPRIS_DATA_ADDRESS, info.getName());
                    intent.putExtra(Constants.MPRIS_DATA_SERVICE, info.getName());

                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                }

            }

            @Override
            public void serviceAdded(final ServiceEvent event) {
                //final ServiceInfo info = mDNS.getServiceInfo(event.getType(),event.getName(), true);
                //internalServiceAdded(info);
            }

            @Override
            public void serviceRemoved(ServiceEvent event) {
                Log.d("MprisBonjour", "Service for Type removed: "+event.getType()+": "+event.getInfo().getType());

                Log.d("MprisBonjour", "App: '" + event.getInfo().getApplication()+"'");

                if(event.getInfo().getApplication().equals(MPRIS_ACCESS_POINT_TYPE_NAME)) {

                    Log.d("MprisBonjour", "MPRIS Access Point Deleted: "+event.getInfo().getName());

                    Intent intent = new Intent(Constants.MPRIS_SERVICE_RESULT_AP_DISAPPEARED);
                    intent.putExtra(Constants.MPRIS_DATA_ADDRESS, event.getInfo().getName());
                    intent.putExtra(Constants.MPRIS_DATA_SERVICE, event.getInfo().getName());

                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                }

            }

            @Override
            public void serviceResolved(ServiceEvent event) {
                Log.d("MprisBonjour", "Service for Type resolved: "+event.getType()+": "+event.getInfo().getURLs()[0]);

                final ServiceInfo info = event.getInfo();

                if(info.getApplication().equals(MPRIS_ACCESS_POINT_TYPE_NAME)) {

                    final String uri = info.getURLs()[0];

                    Log.d("MprisBonjour", "MPRIS Access Point: "+uri);

                    Intent intent = new Intent(Constants.MPRIS_SERVICE_RESULT_AP_DISCOVERED);
                    intent.putExtra(Constants.MPRIS_DATA_MPRISAP_URL, uri);
                    intent.putExtra(Constants.MPRIS_DATA_ADDRESS, info.getName());
                    intent.putExtra(Constants.MPRIS_DATA_SERVICE, info.getName());

                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                }

            }
        }

        private Listener2 listener2;

        private class Listener implements ServiceTypeListener {

            @Override
            public void serviceTypeAdded(ServiceEvent event) {

                Log.d("MprisBonjour", "Type: "+event.getType());

                if(event.getType().substring(1,8).equals(MPRIS_ACCESS_POINT_TYPE_NAME)) {
                    listener2 = new Listener2();
                    mDNS.addServiceListener(event.getType(), listener2);
                }
            }

            @Override
            public void subTypeForServiceTypeAdded(ServiceEvent event) {
                Log.d("MprisBonjour", "SubType: "+event.getType());
            }
        }

        private Listener listener;

        private InetAddress getInetAddress() throws IllegalStateException {

            WifiInfo connInfo = connectionMgr.getWifiManager().getConnectionInfo();

            int myRawAddress = connInfo.getIpAddress();

            byte[] myAddressBytes = new byte[] {
                    (byte) (myRawAddress & 0xff),
                    (byte) (myRawAddress >> 8 & 0xff),
                    (byte) (myRawAddress >> 16 & 0xff),
                    (byte) (myRawAddress >> 24 & 0xff)
            };

            InetAddress myAddress;

            try {
                myAddress = InetAddress.getByAddress(myAddressBytes);
            } catch (UnknownHostException e) {
                throw new IllegalStateException();
            }

            return myAddress;
        }

        public DiscoveryService() {
            /*es = AsyncTask.SERIAL_EXECUTOR;*/
        }

        @Override
        protected void run() throws Exception {

            android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

            running = true;

            // Create and start the HandlerThread - it requires a custom name
            handlerThread = new HandlerThread("DiscoveryServiceHandlerThread");
            handlerThread.start();

            // Get the looper from the handlerThread
            // Note: this may return null
            looper = handlerThread.getLooper();

            // Create a new handler - passing in the looper to use and this class as
            // the message handler
            handler = new Handler(looper, DiscoveryService.this);

            Log.d("MprisBonjour","Discovery initialized.");

            initializeDiscovery();

            while (running) {
                synchronized (mutex) {
                    try {
                        mutex.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }

            // Tell the handler thread to quit
            looper.quit();
            looper = null;

            handlerThread.quit();

            try {
                handlerThread.join();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            Log.d("MprisBonjour","Discovery stopped.");
        }

        private void stopDiscovery() {

            if(mDNS!=null)
            try {
                mDNS.removeServiceTypeListener(listener);
                mDNS.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        private void initializeDiscovery() {

            try {
                listener = new Listener();
                mDNS = JmDNS.create(getInetAddress());
                mDNS.addServiceTypeListener(listener);
            } catch (IOException e) {
                mDNS = null;
                e.printStackTrace();
            }

        }

        @Override
        protected void startUp() throws Exception {
        }

        @Override
        protected void shutDown() throws Exception {
        }

        @Override
        protected void triggerShutdown() {

            if(running) {
                running = false;

                synchronized (mutex) {
                    mutex.notifyAll();
                }
            }

        }

        @Override
        protected Executor executor() {
            return es;
        }

        @Override
        protected String serviceName() {
            return getClass().getSimpleName();
        }
    }

    private class ConnectionManager {

        private BroadcastReceiver connectivityReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
            }
        };

        private WifiManager wifiManager;
        private WifiManager.MulticastLock multicastLock;

        public ConnectionManager() {

            wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

            registerReceiver(connectivityReceiver,
                    new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }

        public WifiManager getWifiManager() {
            return wifiManager;
        }

        public void shutdown() {
            if(connectivityReceiver!=null) {
                try {
                    unregisterReceiver(connectivityReceiver);
                    connectivityReceiver = null;
                } catch( IllegalArgumentException e ) {
                    e.printStackTrace();
                }
            }
        }

        public boolean isWifiEnabled() {
            return wifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLED;
        }

        public void enableMulticastLock() {
            multicastLock = wifiManager.createMulticastLock(ConnectionManager.class.getName());
            multicastLock.setReferenceCounted(true);
            multicastLock.acquire();
        }

        public void disableMulticastLock() {
            if(multicastLock==null) {
                throw new IllegalStateException();
            }

            multicastLock.release();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private final int startFlags = START_STICKY;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if(intent==null||intent.getAction()==null)
            return startFlags;

        final String action = intent.getAction();

        if(action.equals(Constants.MPRIS_DISCOVER_ACTION_STOP)) {
            if(srv!=null) {
                srv.triggerShutdown();
            }
        } else if (action.equals(Constants.MPRIS_DISCOVER_ACTION_START)) {

            if(srv!=null) {
                srv.triggerShutdown();
            }

            srv = new DiscoveryService();
            srv.startAsync();

//            Message msg = srv.handler.obtainMessage();
//            msg.setTarget(srv.handler);
//            msg.sendToTarget();
        }

        return startFlags;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        connectionMgr = new ConnectionManager();
        connectionMgr.enableMulticastLock();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        connectionMgr.disableMulticastLock();
        connectionMgr.shutdown();
    }
}
