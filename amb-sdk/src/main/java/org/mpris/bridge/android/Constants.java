package org.mpris.bridge.android;

/**
 * Created by milosz on 01.10.14.
 */
public class Constants {
    public final static String MPRIS_PKG_PREFIX = "org.mpris.bridge.android";

    public final static String MPRIS_DATA_MPRISAP_URL = MPRIS_PKG_PREFIX + ".data.MPRISAP_URL";
    public final static String MPRIS_DATA_SERVICE = MPRIS_PKG_PREFIX + ".data.SERVICE";
    public final static String MPRIS_DATA_ADDRESS = MPRIS_PKG_PREFIX + ".data.ADDRESS";

    public final static String MPRIS_SERVICE_ACTION_VOLUME = MPRIS_PKG_PREFIX + ".action.ACTION_VOLUME";
    public final static String MPRIS_SERVICE_ACTION_PREVIOUS = MPRIS_PKG_PREFIX + ".action.ACTION_PREVIOUS";
    public final static String MPRIS_SERVICE_ACTION_NEXT = MPRIS_PKG_PREFIX + ".action.ACTION_NEXT";
    public final static String MPRIS_SERVICE_ACTION_PLAYPAUSE = MPRIS_PKG_PREFIX + ".action.ACTION_PLAYPAUSE";
    public final static String MPRIS_SERVICE_ACTION_RAISE = MPRIS_PKG_PREFIX + ".action.ACTION_RAISE";

    public final static String MPRIS_AP_ACTION_MONITOR_SERVER = MPRIS_PKG_PREFIX + ".action.ACTION_MONITOR_SERVER";
    public final static String MPRIS_AP_ACTION_REMOVE_MONITOR = MPRIS_PKG_PREFIX + ".action.ACTION_REMOVE_MONITOR";
    public final static String MPRIS_AP_ACTION_DISCOVER = MPRIS_PKG_PREFIX + ".action.ACTION_DISCOVER";
    public final static String MPRIS_AP_ACTION_WATCH = MPRIS_PKG_PREFIX + ".action.ACTION_WATCH";
    public final static String MPRIS_AP_ACTION_IGNORE = MPRIS_PKG_PREFIX + ".action.ACTION_IGNORE";

    public final static String MPRIS_DISCOVER_ACTION_START = MPRIS_PKG_PREFIX + ".action.ACTION_START_DISCOVERY";
    public final static String MPRIS_DISCOVER_ACTION_STOP = MPRIS_PKG_PREFIX + ".action.ACTION_STOP_DISCOVERY";

    public final static String MPRIS_SERVICE_RESULT_AP_DISCOVERED = MPRIS_PKG_PREFIX + ".action.ACTION_AP_DISCOVERED";
    public final static String MPRIS_SERVICE_RESULT_AP_DISAPPEARED = MPRIS_PKG_PREFIX + ".action.ACTION_AP_DISAPPEARED";

    public final static String MPRIS_RPC_AP_INTERFACE = "mprisap";
    public final static String MPRIS_RPC_SERVER_INTERFACE = "mpris";
}
