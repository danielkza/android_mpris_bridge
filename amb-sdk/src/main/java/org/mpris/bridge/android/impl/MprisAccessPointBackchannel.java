package org.mpris.bridge.android.impl;

import android.os.Looper;
import android.util.Log;

import com.google.gson.JsonObject;

import org.json.rpc.server.JsonRpcExecutor;
import org.json.rpc.server.JsonRpcServerTransport;

import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.iki.elonen.NanoHTTPD;

/**
* Created by milosz on 01.10.14.
*/
public class MprisAccessPointBackchannel extends NanoHTTPD {

    public MprisAccessPointBackchannel(final List<RpcBackchannelInfo> infos) throws IOException {
        super("0.0.0.0", 3339);
        executor = bind(infos);
    }

    private JsonRpcExecutor executor;

    private JsonRpcExecutor bind(final List<RpcBackchannelInfo> infos) {

        JsonRpcExecutor executor = new JsonRpcExecutor();

        for(RpcBackchannelInfo info : infos ) {

            Log.d("MprisMultiplexingBackchannel","Adding "+
                info.ifaceRemoteName + " with "+info.marshaller+" for "+info.iface);

            executor.addHandler(info.ifaceRemoteName, info.marshaller, info.iface);
        }

        return executor;
    }

    @Override
    public Response serve(final IHTTPSession session) {

        long cl = Integer.valueOf(session.getHeaders().get("content-length"));

        Log.d("MprisBackchannelServer","content-length:"+cl);

        final Map<String, String> files = new HashMap<String, String>();
        try {
            session.parseBody(files);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ResponseException e) {
            e.printStackTrace();
        }

        Log.d("MprisBackchannelServer","json: "+files.get("postData"));

        final StringBuilder response = new StringBuilder();

        try {
            for(String method : executor.listMethods())
                Log.d("MprisBackchannelServer", "Method: "+method);

            executor.execute(new JsonRpcServerTransport() {

                @Override
                public Looper getTarget() {
                    return Looper.getMainLooper();
                }

                @Override
                public String readRequest() throws Exception {
                    return files.get("postData");
                }

                @Override
                public void writeResponse(String responseData) throws Exception {
                }

                @Override
                public void provideRequest(JsonObject req) {
                    request = req;
                }
            });
        } catch( Exception e ) {
            Log.e("MprisBackchannelServer","error",e);
        }


        JsonObject resp = new JsonObject();
        resp.addProperty("jsonrpc", "2.0");
        resp.add("result", new JsonObject());
        resp.add("id", request.get("id"));

        Response res = new Response(Response.Status.OK,"application/json",resp.toString());
        return res;
    }

    private JsonObject request;
}
