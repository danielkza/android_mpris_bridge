package org.mpris.bridge.android.impl;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import org.mpris.bridge.android.Constants;

import org.mpris.bridge.android.interfaces.MprisServerInterface;

import java.net.MalformedURLException;

/**
 * Created by milosz on 28.09.14.
 */
public class MprisServerRpcService extends IntentService {

    private MprisServerInterface proxy;

    public MprisServerRpcService() {
        super("MprisServiceRpcService");
    }

    public static void doAction(final Context ctx, final String action, final String apUrl, final String server) {

        Intent intent = new Intent(ctx, MprisServerRpcService.class);
        intent.setAction(action);
        intent.putExtra(Constants.MPRIS_DATA_SERVICE, server);
        intent.putExtra(Constants.MPRIS_DATA_MPRISAP_URL, apUrl);
        ctx.startService(intent);

    }

    public static void doAction(final Context ctx, final String action, final String apUrl, final String server, double arg) {

        Intent intent = new Intent(ctx, MprisServerRpcService.class);
        intent.setAction(action);
        intent.putExtra(Constants.MPRIS_DATA_SERVICE, server);
        intent.putExtra(Constants.MPRIS_DATA_MPRISAP_URL, apUrl);
        intent.putExtra("volume",arg);
        ctx.startService(intent);

    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if(intent==null||intent.getAction()==null)
            throw new IllegalArgumentException();

        final String action = intent.getAction();

        final RpcServerIfaceResolver resolver = new RpcServerIfaceResolver(intent.
                getStringExtra(Constants.MPRIS_DATA_SERVICE));

        try {
            final String uri = intent.getStringExtra(Constants.MPRIS_DATA_MPRISAP_URL);
            proxy = JsonRpcFactory.newServiceProxy(uri,
                    resolver.getIface(RpcServerIfaceResolver.IFACE_PLAYER));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


        if(action.equals(Constants.MPRIS_SERVICE_ACTION_PLAYPAUSE)) {
            try {
                proxy.playPause();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        } else if(action.equals(Constants.MPRIS_SERVICE_ACTION_NEXT)) {
            try {
                proxy.next();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        } else if(action.equals(Constants.MPRIS_SERVICE_ACTION_PREVIOUS)) {
            try {
                proxy.previous();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        } else if(action.equals(Constants.MPRIS_SERVICE_ACTION_VOLUME)) {
            try {
                proxy.volume(intent.getDoubleExtra("volume", 50));
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
    }
}
