package org.mpris.bridge.android.interfaces;

/**
* Created by milosz on 01.10.14.
*/
public interface MprisServerBackchannelListener {

    public void onMetadata(String server, String title, String artist);
    public void onCoverArt(String server, String coverArtBase64);
    public void onIdentity(String server, String identity, String iconBase64);
    public void onPlaybackStatus(String server, String playbackStatus);

}
