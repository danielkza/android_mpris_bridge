package org.mpris.bridge.android.proxy;

import android.content.Context;

import org.mpris.bridge.android.Constants;
import org.mpris.bridge.android.impl.MprisAccessPointRpcService;
import org.mpris.bridge.android.impl.RpcBackchannelInfo;

import java.io.IOException;
import java.util.List;

/**
 * Created by milosz on 29.09.14.
 */
public class MprisAccessPointProxy extends MprisAbsProxy {

    public MprisAccessPointProxy(final Context ctx, final List<RpcBackchannelInfo> infos,
                                 final String apUrl) throws IOException {

        super(ctx, infos, apUrl);
    }

    public void init() {
        MprisAccessPointRpcService.doAction(getContext(),
                Constants.MPRIS_AP_ACTION_DISCOVER, getApUrl());
    }

    private void watch() {
        MprisAccessPointRpcService.doAction(getContext(),
                Constants.MPRIS_AP_ACTION_WATCH, getApUrl());
    }

    private void ignore() {
        MprisAccessPointRpcService.doAction(getContext(),
                Constants.MPRIS_AP_ACTION_IGNORE, getApUrl());
    }

    @Override
    public void stop() {
        super.stop();
        ignore();
    }

    @Override
    public void start() {
        super.start();
        watch();
    }
}
