package org.mpris.bridge.android.interfaces;

/**
 * Created by milosz on 03.10.14.
 */
public interface MprisServerBackchannelMarshaller extends MprisServerBackchannelDispatcher, MprisServerBackchannelListener {
}
