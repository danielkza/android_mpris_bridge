package net.mderezynski.amb.ui;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import net.mderezynski.amb.R;

import org.mpris.bridge.android.app.MprisBaseControlActivity;
import org.mpris.bridge.android.impl.MprisFilteringServerDispatcher;

/**
 * Created by milosz on 29.09.14.
 */
public class MprisControlActivity extends MprisBaseControlActivity {

    private class ServerListener extends MprisFilteringServerDispatcher {

        public ServerListener(String server) {
            super(server);
        }

        @Override
        public void gotCoverArt(final Bitmap coverArt) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    ImageView imageView = ((ImageView)findViewById(R.id.coverart));
                    imageView.setImageBitmap(coverArt);

                    Animation a = AnimationUtils.loadAnimation(getApplicationContext(),
                            R.anim.scale_cover_in);
                    imageView.startAnimation(a);
                }
            });

        }

        @Override
        public void gotMetadata(final String title, final String artist) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    ((ImageView)findViewById(R.id.coverart)).setImageBitmap(null);

                    TextView tv1 = ((TextView)findViewById(R.id.currenttitle));
                    TextView tv2 = ((TextView)findViewById(R.id.currentartist));

                    tv1.setText(title);
                    tv2.setText(artist);

                    Animation a1 = new AlphaAnimation(0f,1f);
                    a1.setDuration(330);
                    a1.setInterpolator(new AccelerateDecelerateInterpolator());

                    Animation a2 = new AlphaAnimation(0f,1f);
                    a2.setDuration(500);
                    a2.setInterpolator(new AccelerateDecelerateInterpolator());

                    tv1.startAnimation(a1);
                    tv2.startAnimation(a2);

                }
            });

        }

        @Override
        public void gotIdentity(final String identity, final Bitmap icon) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    getActionBar().setIcon(new BitmapDrawable(getResources(), icon));
                    getActionBar().setTitle(identity);
                }
            });
        }

        @Override
        public void gotPlaybackStatus(final String playbackStatus) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ImageView b_pause = (ImageView) findViewById(R.id.ctrl_pause);

                    if(playbackStatus.equals("Playing")) {
                        b_pause.setImageResource(R.drawable.amb_ic_play);
                    } else if(playbackStatus.equals("Paused")) {
                        b_pause.setImageResource(R.drawable.amb_ic_pause);
                    }
                }
            });

        }

    }

    @Override
    protected MprisFilteringServerDispatcher getListener(final String server) {
        return new ServerListener(server);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_mpris_control);

        AutofitTextView aftv = ((AutofitTextView)findViewById(R.id.currenttitle));
        aftv.setMaxLines(1);

        aftv = ((AutofitTextView)findViewById(R.id.currentartist));
        aftv.setMaxLines(1);

        ImageView b_pause = (ImageView) findViewById(R.id.ctrl_pause);
        b_pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getProxy().playPause();
            }
        });

        ImageView b_next = (ImageView) findViewById(R.id.ctrl_next);
        b_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getProxy().next();
            }
        });

        ImageView b_previous = (ImageView) findViewById(R.id.ctrl_previous);
        b_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getProxy().previous();
            }
        });

        SeekBar seek = (SeekBar) findViewById(R.id.volume);

        seek.setInterpolator(new DecelerateInterpolator());

        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            private long lastEventTime = System.currentTimeMillis();

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                final long currentEventTime = System.currentTimeMillis();

                if((currentEventTime-lastEventTime)>100){
                    getProxy().setVolume(((double) (i / 100.)));
                    lastEventTime = System.currentTimeMillis();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                getProxy().setVolume(((double) (seekBar.getProgress() / 100.)));
                lastEventTime = System.currentTimeMillis();
            }
        });
    }
}
