package net.mderezynski.amb.ui;

import android.content.Context;
import android.content.Intent;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import net.mderezynski.amb.R;

import org.mpris.bridge.android.Constants;
import org.mpris.bridge.android.discovery.MprisDiscoveryManager;
import org.mpris.bridge.android.app.MprisBaseDiscoveryActivity;
import org.mpris.bridge.android.interfaces.MprisAccessPointBackchannelListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A login screen that offers login via email/password.

 */
public class MprisDiscoveryActivity extends MprisBaseDiscoveryActivity {

    private ServerListAdapter serverDiscoveryListener;
    private AccessPointAdapter accessPointDiscoveryListener;
    private MprisDiscoveryManager discoveryManager;

    private class AccessPointAdapter extends BaseAdapter implements MprisDiscoveryManager.DiscoveryListener {

        private final List<MprisDiscoveryManager.MprisApHostInfo> accessPoints = new ArrayList<MprisDiscoveryManager.MprisApHostInfo>();
        private final Spinner accessPointSpinner;

        public AccessPointAdapter(final Spinner spinner) {
            this.accessPointSpinner = spinner;
        }

        @Override
        public void onDiscoveryStarted() {
            accessPoints.clear();
            notifyDataSetChanged();
        }

        @Override
        public void onDiscoveryStopped() {
        }

        @Override
        public void onAccessPointDiscovered(MprisDiscoveryManager.MprisApHostInfo info) {

            if(accessPointSpinner.getSelectedItemPosition()>Spinner.INVALID_POSITION&&mprisApExists(info)) {
                MprisDiscoveryManager.MprisApHostInfo currentInfo = (MprisDiscoveryManager.MprisApHostInfo) getItem(accessPointSpinner.getSelectedItemPosition());
                if(currentInfo.equals(info)) {
                    serverDiscoveryListener.clear();
                    getProxy().init();
                }
            } else {
                addMprisAp(info);
            }
        }

        @Override
        public void onAccessPointDisappeared(MprisDiscoveryManager.MprisApHostInfo info) {
            Log.d("MprisBonjour", "MPRIS AP Disappeared: " + info.uri);

            if(accessPointSpinner.getSelectedItemPosition()>Spinner.INVALID_POSITION&&mprisApExists(info)) {
                MprisDiscoveryManager.MprisApHostInfo currentInfo = (MprisDiscoveryManager.MprisApHostInfo) getItem(accessPointSpinner.getSelectedItemPosition());
                if(currentInfo.equals(info)) {
                    getProxy().shutdown();
                }
            }

            delMprisAp(info);
        }

        public void addMprisAp(final MprisDiscoveryManager.MprisApHostInfo info) {
            accessPoints.add(info);
            notifyDataSetChanged();
        }

        public void delMprisAp(final MprisDiscoveryManager.MprisApHostInfo info) {
            accessPoints.remove(info);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return accessPoints.size();
        }

        @Override
        public Object getItem(int i) {
            return accessPoints.get(i);
        }

        @Override
        public long getItemId(int i) {

            if(i<0 || i >= accessPoints.size())
                return -1;

            return accessPoints.indexOf(accessPoints.get(i));
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            if(view==null) {
                view = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(android.R.layout.simple_list_item_1, viewGroup, false);
            }

            ((TextView)view.findViewById(android.R.id.text1)).setText(((MprisDiscoveryManager.MprisApHostInfo)getItem(i)).hostName);

            return view;
        }

        public boolean mprisApExists(final MprisDiscoveryManager.MprisApHostInfo info) {
            return accessPoints.contains(info);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discovery);

        final ListView serverList = (ListView) findViewById(R.id.services);
        final Spinner accessPointSpinner = (Spinner) findViewById(R.id.spinner2);

        serverList.setOnItemClickListener(new AbsListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                ServerListAdapter.HostInfo serverListAdapterHostInfo = serverDiscoveryListener.getItem(i);
                MprisDiscoveryManager.MprisApHostInfo info = (MprisDiscoveryManager.MprisApHostInfo) accessPointDiscoveryListener.getItem(accessPointSpinner.getSelectedItemPosition());

                Intent intent = new Intent(getApplicationContext(), MprisControlActivity.class);
                intent.putExtra(Constants.MPRIS_DATA_SERVICE, serverListAdapterHostInfo.server);
                intent.putExtra(Constants.MPRIS_DATA_MPRISAP_URL, info.uri.toString());
                startActivity(intent);

            }
        });


        accessPointSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                MprisDiscoveryManager.MprisApHostInfo info = (MprisDiscoveryManager.MprisApHostInfo) accessPointDiscoveryListener.getItem(i);
                if (switchProxy(info.uri.toString())) {
                    serverDiscoveryListener.clear();
                    getProxy().init();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                serverDiscoveryListener.clear();
            }
        });

        accessPointDiscoveryListener = new AccessPointAdapter(accessPointSpinner);
        serverDiscoveryListener = new ServerListAdapter((LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE));

        serverList.setAdapter(serverDiscoveryListener);
        accessPointSpinner.setAdapter(accessPointDiscoveryListener);

        discoveryManager = new MprisDiscoveryManager(getApplicationContext(), accessPointDiscoveryListener);
    }


    @Override
    protected MprisAccessPointBackchannelListener getAccessPointListener() {
        return serverDiscoveryListener;
    }

    @Override
    protected void onResume() {
        super.onResume();
        discoveryManager.start(getApplicationContext());
    }

    @Override
    protected void onPause() {
        super.onPause();
        discoveryManager.stop(getApplicationContext());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        discoveryManager.shutdown(getApplicationContext());
    }
}



