package net.mderezynski.amb.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

/**
 * Created by milosz on 15.09.14.
 */
public abstract class BaseListAdapter extends BaseAdapter {

    protected final LayoutInflater mInflater;

    public BaseListAdapter(final LayoutInflater inflater) {
        mInflater = inflater;
    }

    @Override
    public final View getView(int position, View view, ViewGroup viewGroup) {

        if (view == null) {
            view = newView(viewGroup);
        }

        bindView(position, view);

        return view;
    }

    public abstract View newView(ViewGroup viewGroup);

    public abstract void bindView(int position, View view);

}